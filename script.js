$(document).ready(function () {
	$(".esr-form-builder").esrCreateForm({
		inputs: {
			text: {
				id: "text",
				title: "Text",
				description: "Text input",
				type: "input",
				subtype: "text",
				is_addable: true
			},
			email: {
				id: "email",
				title: "Email",
				description: "Email input",
				type: "input",
				subtype: "email",
				is_addable: false
			},
			checkbox: {
				id: "checkbox",
				title: "Checkbox",
				description: "",
				type: "checkbox",
				is_addable: true
			},
			select: {
				id: "select",
				title: "Select",
				description: "",
				type: "select",
				is_addable: true
			},
			textarea: {
				id: "textarea",
				title: "TextArea",
				description: "Area for notes",
				type: "textarea",
				is_addable: true
			}
		},
		default: [
			{
				columns: 2,
				is_removable: false,
				elements: [
					{
						title: "Name",
						type: "input",
						subtype: "text",
						is_removable: false
					},
					{
						title: "Surname",
						type: "input",
						subtype: "text",
						is_removable: false
					}]
			},
			{
				columns: 1,
				is_removable: false,
				elements: [
					{
						title: "Email",
						type: "input",
						subtype: "email",
						is_removable: false
					}]
			},
			{
				columns: 1,
				elements: [
					{
						title: "Note",
						type: "textarea"
					}]
			}
		]
	});
});