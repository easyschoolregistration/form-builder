(function ($) {
	$.fn.esrCreateForm = function (settings) {
		var builderElm = $("<div class='esr-form-placer'><div class='esr-form-placer-elements'></div><div class='esr-add-rows'><i class='fas fa-plus'></i> Add Row <ul class='esr-add-row-options'><li class='esr-add-row' data-columns='1'>One Column</li><li class='esr-add-row' data-columns='2'>Two Columns</li></ul></div></div>").appendTo(this);
		var inputsElm = $("<ul class='esr-inputs'></ul>").appendTo(this);
		$.each(settings.inputs, function (ik, iv) {
			if (iv.is_addable) {
				$("<li class='esr-input' data-id='" + iv.id + "'><span class='esr-input-title'>" + iv.title + "</span></li>").appendTo(inputsElm);
			}
		});

		esrBindFunctions(builderElm, settings.inputs);

		$.each(settings.default, function (dk, dv) {
			var row = esrAddRowElement(dv.columns, dv);
			if (dv.columns === 1) {
				esrAddElement($(".esr-column", $(row)), dv.elements[0]);
			} else {
				esrAddElement($(".esr-column", $(row))[0], dv.elements[0]);
				esrAddElement($(".esr-column", $(row))[1], dv.elements[1]);
			}
		});

		return this;
	};

	function esrBindFunctions(builderElm, inputsSettings) {
		$("body").on("click", ".esr-input", function () {
			$(".esr-inputs").hide();
			var column = $(this).closest(".esr-column");
			$(".esr-add-element", column).hide();
			if ($(".esr-element", column).length === 0) {
				esrAddElement(column, inputsSettings[$(this).data("id")]);
			}
		}).on("click", ".esr-element-button.remove", function () {
			$(".esr-add-element", $(this).closest(".esr-column")).show();
			$(this).closest(".esr-element").remove();
		}).on("click", ".esr-element-button.edit", function () {
			$(this).closest(".esr-element").find(".esr-element-editor").toggle();
		}).on("change keyup", "input[name=editor-name]", function () {
			$(this).closest(".esr-element").find(".esr-element-title").text($(this).val());
		}).on("change keyup", "textarea[name=editor-description]", function () {
			$(this).closest(".esr-element").find(".esr-element-description").text($(this).val());
		}).on("click", ".esr-add-rows:not(.esr-add-row)", function () {
			$(this).find(".esr-add-row-options").toggle();
		}).on("click", ".esr-add-row", function () {
			esrAddRowElement($(this).data("columns"));
		}).on("click", ".esr-remove-row", function () {
			$(this).parent().remove();
		}).on("click", ".esr-add-element-title", function () {
			if ($(this).closest(".esr-column").find(".esr-element").length === 0) {
				var inputsElement = $(".esr-inputs");
				$(this).parent().append(inputsElement);
				inputsElement.show();
			}
		}).on("change", "input[name=email-confirmation]", function () {
			var row = $(this).closest(".esr-row");
			if ($(this).is(":checked")) {
				$(row).removeClass("esr-columns-1").addClass("esr-columns-2");
				esrAddElement($("<div class='esr-column'></div>").appendTo(row), {
					title: "Email Confirmation",
					type: "input",
					subtype: "email",
					is_removable: false,
					is_editable: false
				});
			} else {
				$(row).removeClass("esr-columns-2").addClass("esr-columns-1").find(".esr-column:last-child").remove();
			}
		})/*.on("drop", ".esr-column", function (e) {
			e.preventDefault();
			var data = e.originalEvent.dataTransfer.getData("text");
			e.target.appendChild(document.getElementById(data));
		}).on("dragover", ".esr-column", function (e) {
			e.preventDefault();
		}).on("dragstart", ".esr-element", function (e) {
			e.originalEvent.dataTransfer.setData("text", e.target.id);
		})*/;
	}

	function esrAddElement(column, inputSettings) {
		inputSettings = $.extend({is_editable: true, is_removable: true}, inputSettings);
		switch (inputSettings.type) {
			case "input":
				esrPrintBuilderInput(column, inputSettings);
				break;
			case "checkbox":
				esrPrintBuilderCheckbox(column, inputSettings);
				break;
			case "select":
				esrPrintBuilderSelect(column, inputSettings);
				break;
			case "textarea":
				esrPrintBuilderTextArea(column, inputSettings);
				break;
			default:
			// code block
		}

		$(".esr-add-element", column).hide();
	}

	function esrAddRowElement(columns, rowSettings = {}) {
		rowSettings = $.extend({is_removable: true}, rowSettings);

		var inputHtml = "<div class='esr-row clearfix esr-columns-" + columns + "'>";

		if (rowSettings.is_removable) {
			inputHtml += "<div class='esr-remove-row'><i class='fas fa-minus'></i></div>";
		}

		for (var i = 0; i < columns; i++) {
			inputHtml += "<div class='esr-column'><div class='esr-add-element'><div class='esr-add-element-title'><i class='fas fa-plus'></i> Add Element</div></div></div>";
		}
		inputHtml += "</div>";

		return $(inputHtml).appendTo(".esr-form-placer-elements");
	}

	function esrPrintBuilderInput(builderElm, inputSettings) {
		var inputHtml = "<div id='" + esrUniqId() + "' class='esr-element' /*draggable='true*/'>";
		inputHtml += esrPrintBuilderElementTitle(inputSettings);
		inputHtml += "<input type='" + inputSettings.subtype + "' value=''>";
		inputHtml += esrPrintBuilderElementEditor(inputSettings);
		inputHtml += "</div>";

		var input = $(inputHtml).appendTo(builderElm);
		esrAddEditButtons(input, inputSettings);
	}

	function esrPrintBuilderCheckbox(builderElm, inputSettings) {
		var inputHtml = "<div id='" + esrUniqId() + "' class='esr-element' /*draggable='true*/'>";
		inputHtml += "<label><input type='checkbox'> <span class='esr-element-title'>" + inputSettings.title + "</span></label>";
		inputHtml += esrPrintBuilderElementEditor(inputSettings);
		inputHtml += "</div>";

		var input = $(inputHtml).appendTo(builderElm);
		esrAddEditButtons(input, inputSettings);
	}

	function esrPrintBuilderSelect(builderElm, inputSettings) {
		var inputHtml = "<div id='" + esrUniqId() + "' class='esr-element' /*draggable='true*/'>";
		inputHtml += esrPrintBuilderElementTitle(inputSettings);
		inputHtml += "<select><option>- select option -</option></select>";
		inputHtml += esrPrintBuilderElementEditor(inputSettings);
		inputHtml += "</div>";

		var input = $(inputHtml).appendTo(builderElm);
		esrAddEditButtons(input, inputSettings);
	}

	function esrPrintBuilderTextArea(builderElm, inputSettings) {
		var inputHtml = "<div id='" + esrUniqId() + "' class='esr-element' /*draggable='true*/'>";
		inputHtml += esrPrintBuilderElementTitle(inputSettings);
		inputHtml += "<textarea></textarea>";
		inputHtml += esrPrintBuilderElementEditor(inputSettings);
		inputHtml += "</div>";

		var input = $(inputHtml).appendTo(builderElm);
		esrAddEditButtons(input, inputSettings);
	}

	function esrPrintBuilderElementTitle(inputSettings) {
		return "<div class='esr-element-title'>" + inputSettings.title + "</div><div class='esr-element-description'></div>";
	}

	function esrPrintBuilderElementEditor(inputSettings) {
		var editorHtml = "";
		editorHtml += "<div class='esr-element-editor-name'><input name='editor-name' type='text' value='" + inputSettings.title + "'></div>";
		editorHtml += "<div class='esr-element-editor-description'><textarea name='editor-description'></textarea></div>";

		if (inputSettings.subtype === "email") {
			editorHtml += "<label><input name='email-confirmation' type='checkbox'> Enable email confirmation</label>";
		}

		return "<div class='esr-element-editor'>" + editorHtml + "</div>";
	}

	function esrAddEditButtons(input, inputSettings) {
		var buttonsHTML = "<div class='esr-element-buttons'><ul>";

		if (inputSettings.is_editable) {
			buttonsHTML += "<li class='esr-element-button edit'><i class='fas fa-pencil-alt'></i></li>";
		}
		if (inputSettings.is_removable) {
			buttonsHTML += "<li class='esr-element-button remove'><i class='fas fa-times'></i></li>";
		}

		buttonsHTML += "</ul></div>";
		input.append(buttonsHTML);
	}

	function esrUniqId() {
		return Math.round(new Date().getTime() + (Math.random() * 100));
	}

}(jQuery));